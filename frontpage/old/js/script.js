class Color {
    constructor(options) {
        this._change_speed = 4;

        var col = Color.parseColor(options);
        this.r = col.r;
        this.g = col.g;
        this.b = col.b;
    }

    goTo(color) {
        this.next_col = Color.parseColor(color);
    }

    update() {
        if (typeof(this.next_col) !== 'undefined') {
            for (var c in this.next_col) {
                if (this.next_col[c] > this[c]) {
                    this[c] += this._change_speed;
                } else if (this.next_col[c] < this[c]) {
                    this[c] -= this._change_speed;
                }
            }
        }
    }

    static parseColor(color) {
        var out = new Object();
        if (typeof(color) === 'object') {
            out.r = options.r;
            out.g = options.g;
            out.b = options.b;
        } else {
            var col = Color.hex2rgb(color);
            out.r = col.r;
            out.g = col.g;
            out.b = col.b;
        }
        return out;
    }

    static hex2rgb(hex) {
        var hex = hex.replace('#', '');
        var len = hex.length/3;
        var col = {
            r: parseInt(hex.substr(0*len, len), 16),
            g: parseInt(hex.substr(1*len, len), 16),
            b: parseInt(hex.substr(2*len, len), 16)
        };
        return col;
    }

    toString() {
        this.update();
        return 'rgb('+this.r+','+this.g+','+this.b+')';
    }
}

class Cloud {
    constructor(x_max, y_max) {
        this._x_max = x_max;
        this._y_max = y_max;
        this._color = '#fff';
        this.v = Math.random() * (2 - 0.5) + 0.5;
        this.x = Math.random() * this._x_max;
        this.y = Math.random() * this._y_max;

        this.path = new Path2D();
        this.gen_path();
    }

    gen_path() {
        var radius = Math.random() * (50-35) + 35,
            x = (Math.random() * (100 - radius)) + radius,
            y = (Math.random() * (100 - radius)) + radius,
            c = Math.random() * (5 - 2) + 2,
            xy;
        for (var i = 0; i < c; i++) {
            xy = Cloud.get_pos(x, y, radius);
            x = xy.x;
            y = xy.y;
            this.path.arc(x, y, radius, 0, 2*Math.PI);
            this.path.closePath();
        }
    }

    static get_pos(ox, oy, or) {
        var randomAngle = Math.random() * 2 * Math.PI;

        return {
            x: Math.sin(randomAngle) * 0.7 * or,
            y: Math.cos(randomAngle) * 0.7 * or
        };
    }

    draw(ctx) {
        ctx.save();
        ctx.translate(this.x, this.y);

        ctx.fillStyle = this._color;
        ctx.fillStyle = '#fff';
        ctx.shadowColor = 'rgba(0, 0, 0, 0.3)';
        ctx.shadowOffsetX = -10;
        ctx.shadowOffsetY = 38;

        ctx.fill(this.path);

        ctx.restore();

        this.x += this.v;
        this.y += this.v;
        if (this.x > (this._x_max + 100)) {
            this.x = -100;
        }
        if (this.y > (this._y_max + 100)) {
            this.y = -100;
        }
    }
}






var islands = {
    texel: {
        path: new Path2D('m 17.951479,102.94219 c 21.3,-29.200001 51.9,-56.700001 57.4,-60.900001 5.5,-4.2 38.300001,-37.3999996 47.400001,-41.09999955 9,-3.60000005 11.7,15.29999955 14.6,18.09999955 2.9,2.8 -6.5,12.7 -1.2,13.1 5.3,0.4 6.8,8.3 8.4,15 2.1,8.6 -0.9,15.9 -4.8,17.9 -3.9,2 -7.9,33.2 -11.5,40.300001 -3.6,7.1 -11.2,5.4 -15,9.4 -3.8,4 -5.1,5.3 -7.4,8.8 -2.3,3.4 -1.6,6 -5.8,7.9 -4.200001,1.9 -2.800001,2.9 -5.900001,5.5 -3,2.5 -12.8,10 -18.5,8.8 -5.7,-1.2 -16,5.8 -22.2,10.2 -6.2,4.4 -5.4,4.9 -5,6.1 0.4,1.1 2.7,6.1 -5.6,3.2 -8.3,-2.9 -17.9,-14.9 -18.6,-12.1 -0.8,2.8 13.8,13.8 8,16.6 0,0 -21.3,1.3 -27.8000003,1.9 -8.3,0.8 -0.6,-7.9 -2.5,-20.8 -1.79999988,-13 16.0000003,-47.9 16.0000003,-47.9 z'),
        width: 145.74268,
        height: 172.19403,
        dy: 0,
        color: new Color('#000')
    },
    vlieland: {
        path: new Path2D('m 12.447376,32.087047 c 24.6,-4.6 63.7,-17.4 67.9,-19.2 4.2,-1.8 45.100004,-13.49999987 62.500004,-12.29999987 17.4,1.19999997 14.8,-1 25.1,7.29999997 10.3,8.2999999 3.4,6.1999999 0.3,7.0999999 -3.1,0.9 -2.3,1.7 -6.5,0.5 -4.2,-1.2 -9.8,-4.6 -14.5,-1.7 -4.6,2.8 -12.4,4.4 -19.4,4.3 -6.9,-0.2 -17.3,-0.3 -23.3,-0.5 -6.000004,-0.2 -13.900004,1.1 -15.700004,2 -1.8,0.9 -1,9.1 -1.5,12.2 -0.5,3.1 -2.3,4.7 -8.6,7.1 -6.3,2.4 -14,2.4 -18.3,3.300001 -4.3,0.9 0.2,1.2 -5.1,4.1 -5.3,2.9 -9.7,6.4 -13.4,6.4 -3.7,-0.1 -4,-2.9 -12.1,-4.7 -8.1,-1.8 -9,-4.4 -13.7,-0.7 -4.7,3.7 -4.9,6.5 -9.1000001,4.1 -4.2,-2.3 -8.5,-1.9 -5.6,-11.700001 3,-10 11.0000001,-7.6 11.0000001,-7.6 z'),
        width: 173.89291,
        height: 53.18705,
        dy: -40,
        color: new Color('#000')
    },
    terschelling: {
        path: new Path2D('m 70.867921,0.7209661 c 91.799999,9.1 105.599999,10.2999999 127.799999,9.7999999 22.2,-0.5 28.8,-0.9999999 39.1,3.6 10.3,4.5 15.5,8.2 16.9,13.3 1.4,5.1 -1.1,11.8 -2.2,6.2 -1.1,-5.6 -2,-6.3 -3.2,-5.2 -1.2,1.1 -6,4 -8.4,4.6 -2.4,0.5 -7.7,-1.1 -10.3,-2.5 -2.6,-1.4 -3.5,-2.2 -6.5,-0.5 -3,1.6 -3.3,1.7 -6.1,2.5 -2.8,0.8 -14.3,1.1 -15.8,1.7 -1.5,0.5 -3,0.8 -3.8,2.2 -0.8,1.5 3.1,5.2 3.7,5 0.6,-0.1 1.2,0.8 -3.4,1.6 -4.6,0.7 -13.2,0.2 -20.7,-3.5 -7.6,-3.8 -16.6,-6.1 -23.1,-7.3 -6.5,-1.2 -13.5,-0.7 -21.8,3.1 -8.3,3.8 -11.7,6.5 -16.7,5.7 -5,-0.9 -4.8,-1.1 -7.1,-0.6 -2.3,0.5 -2,0.9 -4.6,0.5 -2.6,-0.5 -2,-3.7 -3.8,-3.9 -1.799999,-0.2 -3.199999,1.3 -3.299999,1.9 -0.1,0.6 -0.7,2.6 -3.8,1.3 -3.1,-1.3 -4.5,-2.6 -4.7,-2.1 -0.2,0.5 -1.1,2.7 -1.7,3.1 -0.6,0.4 -3.8,1.4 -9.1,-2.8 -5.3,-4.2 -3.3,-0.7 -5.7,-1.9 -2.4,-1.3 -2.5,-1.3 -4.4,-2.3 -1.9,-1 -1.4,-1.1 -5.7,1.4 -4.3,2.5 -6.4,1.6 -8.2,0.4 -1.8,-1.2 -13.1,-4.8 -13.3,2.7 -0.3,7.5 -6.7,2.4 -9.1,3.6 -2.4,1.2 -2.8,5.1 -12.3,1.6 -9.5,-3.5 -9.2,-3.9 -10.6000001,-6.9 0,0 -3.7,-2.8 -6.6,-4.3 -2.80000001,-1.5 -2.50000001,-4.9 1.2,-7.7 3.7,-2.8 12.4000001,-9.7 19.4000001,-13.1 7,-3.3999999 12.1,-11.8999999 23.7,-11.3999999 11.7,0 24.2,0.2 24.2,0.2 z'),
        width: 255.59062,
        height: 45.84409,
        dy: -30,
        color: new Color('#000')
    },
    ameland: {
        path: new Path2D('m 59.957014,19.883817 c 6.2,6.8 9.8,11.3 37.5,18.2 27.799996,6.9 32.999996,8.3 48.099996,13.2 0,0 11.1,3.2 26,8.3 15,5.2 23.8,8.300007 29,12.300007 5.2,4 4.8,3.3 4,5.8 -0.7,2.5 -1.9,3.9 -5.8,4.2 -3.9,0.3 -4.9,1.7 -12.4,-1.7 -7.5,-3.4 11.4,-0.9 -17,-9.7 -6.6,-2 -5.3,0.6 -17,-0.2 -11.7,-0.8 -6,-0.8 -10.8,-3 -4.9,-2.100007 5.4,5 -6.7,1.6 -12,-3.400007 -29.5,-1.800007 -36.199996,-5.200007 -6.7,-3.4 -28.4,-15.1 -32.1,-18.4 -3.8,-3.2 -6.9,-1.5 -9.3,1.1 -2.4,2.6 -12,4 -18.3,3.6 -6.3,-0.4 -2.3,1.9 -14.8,-0.5 -12.6,-2.4 -17.5999998,-13 -21.3999998,-19 -3.8,-5.9 -2.09999996,-14.9 0,-18.4 2.1,-3.5000001 12.3999998,-9.1000001 16.6999998,-10.5000001 4.3,-1.40000007 7.5,-2.30000007 18.3,2.9 10.8,5.1 8,4.4 13.1,6.7000001 4.9,2.5 9.1,8.7 9.1,8.7 z'),
        width: 205.52446,
        height: 82.90878,
        dy: 45,
        color: new Color('#000')
    },
    schiermonnikoog: {
        path: new Path2D('m 52.22533,10.698853 c 20.8,14.7 22.5,17.4 34.5,28 12,10.6 25.2,18.3 29.1,19.7 3.9,1.4 8.60002,8.5 13.40002,9.3 4.8,0.8 7.1,2.8 11.9,6.8 4.8,4 6.8,6.3 6.6,7.9 -0.2,1.5 -6.5,2.4 -8.9,2.5 -2.4,0.1 -7,-1.3 -10.3,-3.1 -3.3,-1.8 -3.5,-6.8 -13.90002,-8.8 -10.4,-2 -11.2,-2.1 -15.7,-5.2 -4.5,-3.1 -5.8,-4.9 -10.8,-3.5 -5,1.4 -6.1,1.3 -13.9,-1 0,0 -10,-5.1 -14.4,-9.3 -4.4,-4.2 -11.4,-5.5 -17.1,-9.7 -5.8,-4.3 -11.5,-10.5 -13.3,-11.8 -1.8,-1.3 -3,-3.5 -8.6,0.5 -5.6,4 -12.2000005,3.6 -14.4000005,5.2 -2.2,1.5 -4.7,6.3 -5.59999997,-0.6 -0.9,-6.9 0,-7.2 3.59999997,-11.6 3.6,-4.4 6.2000005,-0.8 7.7000005,-2.1 1.5,-1.3 -5.1000005,-0.5 3.1,-7.4 8.2,-6.8999997 8.2,-6.8999997 8.2,-6.8999997 0,0 15.9,-1.3 17.3,-1.2 1.4,0.1 2.2,-1.8 -0.2,-3.1 -2.4,-1.2 -9.1,-1.4 -4.7,-3.6 4.5,-2.10000004 6,-1.90000004 11,4.3 5,6.1999997 5.4,4.6999997 5.4,4.6999997 z'),
        width: 148.23982,
        height: 85.40428,
        dy: 95,
        color: new Color('#000')
    }
};
var sea_color = {
    bg: '#046E8F',
    fg: '#068AB2'
}




var canvas  = document.getElementById('canvas'),
    ctx     = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;


var space_between_islands = 75,
    space_between_walls = 200,
	space_from_top = 200,
    working_area = (canvas.width-2*space_between_walls),
    total_islands_width = 4*space_between_islands;
for (var island in islands) {
    total_islands_width += islands[island].width;
}
var scaling_factor = (working_area) / total_islands_width;
var debug = false;


ctx.save();

clouds = [];
for (var i = 0; i < 6; i++) {
    clouds.push(new Cloud(canvas.width, canvas.height));
}

canvas.addEventListener('mousemove', function(event) {
  if (event.region) {
    alert('hit region: ' + event.region);
  }
});

animate();

var i = 0;
function animate() {
    setTimeout(function() {
        requestAnimationFrame(animate);
        
        canvas.width = canvas.width;
        ctx.save();
        // ctx.clearRect(0, 0, canvas.width, canvas.height);
        drawSea();

        drawIslands();
        
        for (c in clouds) {
            clouds[c].draw(ctx);
        }

    }, 1000/30);
}

function drawIslands() {
    ctx.save();
	ctx.translate(space_between_walls, space_from_top);
	ctx.scale(scaling_factor, scaling_factor);

	for (var island in islands) {
		ctx.fillStyle = islands[island].color;
		ctx.translate(0, islands[island].dy);
		ctx.fill(islands[island].path);
        ctx.save();

        if (debug == true) {
            ctx.fillStyle = 'rgba(0, 0, 255, 0.4)';
            ctx.fillRect(0, 0, islands[island].width, islands[island].height);
            ctx.fillStyle = 'rgba(200, 0, 255, 0.8)';
            ctx.font = '14px sans-serif';
            ctx.fillText('w: ' + islands[island].width, 5, 15);
            ctx.fillText('h: ' + islands[island].height, 5, 30);
            ctx.fillStyle = 'rgba(100, 0, 100, 0.4)';
            ctx.fillRect(islands[island].width, 0, space_between_islands, 100);
            ctx.fillText('w: ' + space_between_islands, islands[island].width + 5, 15);
        }

        ctx.restore();
		ctx.translate(islands[island].width + space_between_islands, 0);
	}
    ctx.restore();
}

var fac = 1,
    fac_dir = 0;
function drawSea() {
    ctx.save();
    
    // Achtergrond
    ctx.fillStyle = sea_color.bg;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    var height = 100;
    var waves = 3,
        dx = canvas.width / waves;

    // ctx.translate(0, 100);
    // ctx.beginPath();
    // for (var i = 0; i < waves; i++) {
    //     ctx.bezierCurveTo((1/4)*dx + dx*i, 50*fac, (3/4)*dx + dx*i, -50*fac, dx*(i+1), 0);
    // }
    // ctx.lineTo(canvas.width, height);
    // for (var i = 0; i < waves; i++) {
    //     ctx.bezierCurveTo(canvas.width - (1/4)*dx - dx*i, height - 80*fac, canvas.width - (3/4)*dx - dx*i, height + 80*fac, canvas.width - dx*(i+1), height);
    // }
    // ctx.lineTo(0, 0);

    if (fac_dir == 0) {
        fac -= Math.random() * 0.005;
    } else {
        fac += Math.random() * 0.005;
    }
    if (fac > 1) {
        fac_dir = 0;
    } else if (fac < -1) {
        fac_dir = 1;
    }
    if (Math.random() > 0.99) {
        fac_dir = !fac_dir;
    }


    // ctx.closePath();
    ctx.fillStyle = sea_color.fg;
    ctx.strokeStyle = '#fff';
    // ctx.stroke();
    // ctx.fill();

    // ctx.moveTo(0, 500);
    ctx.translate(0, 100);
    for (var i = 0; i < waves; i++) {
        ctx.bezierCurveTo((1/4)*dx + dx*i, 50*fac, (3/4)*dx + dx*i, -50*fac, dx*(i+1), 0);
    }
    ctx.strokeStyle = sea_color.fg;
    ctx.lineWidth = 60;
    ctx.stroke();

    ctx.restore();
}

var requestAnimationFrame =
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		function(callback) {
			return setTimeout(callback, 1000/60);
		};


function dateToSeconds(date) {
    var d = date.split('/');
    return ((new Date(d[2], d[1]-1, d[0])).getTime()) / 1000;
}
