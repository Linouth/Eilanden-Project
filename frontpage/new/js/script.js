$( function() {
    $('.datepicker').datepicker ({
        altFormat: 'dd-mm-yyyy',
        onSelect: onDateSelect
    });

    $('input.check', '#form').change(function() {
        updateIslands();
    });
});

function onDateSelect(date, obj){
    updateIslands();
}

function updateIslands() {
    var dateFrom = $('.datepicker:eq(0)').val().split('/');
    var dateTo = $('.datepicker:eq(1)').val().split('/');
    dateFrom = (new Date(dateFrom[2], dateFrom[1], dateFrom[0])).getTime()/1000;
    dateTo = (new Date(dateTo[2], dateTo[1], dateTo[0])).getTime()/1000;

    var cats = [];
    $('input.check:checked', '#form').each(function() {
        cats.push($(this).attr('name'));
    });

    $.get('api/getcolors/' + dateFrom + '/' + dateTo + '/' + cats.join(), function(data) {
        alert(data.r, data.g, data.b);
    });
}
