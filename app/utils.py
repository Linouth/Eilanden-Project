def inPeriod(cFrom, cTo, oFrom, oTo):
    return ((oFrom <= cFrom <= oTo) or
            (oFrom <= cTo <= oTo) or
            (cFrom <= oFrom <= cTo) or
            (cFrom <= oTo <= cTo))


def scale(val, src, dst):
    """
    Scale the given value from the scale of src to the scale of dst.
    Props to 'Glenn Maynard' on SO

    val - value in range src
    src - range from to (tuple)
    dst - range from to (tuple)
    """
    try:
        out = ((val - src[0]) / (src[1]-src[0])) * (dst[1]-dst[0]) + dst[0]
    except ZeroDivisionError:
        out = 0
    return out
