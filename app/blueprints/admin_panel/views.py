from app import forms, db, app
from app.models import User, Object, Category, Island
from flask import (
        Blueprint, render_template, redirect, flash, request, url_for
)
from flask_login import login_user, logout_user, login_required, current_user
import bcrypt
import time
import hashlib
import os
import datetime


admin_panel = Blueprint('admin_panel', __name__,
                        template_folder='templates',
                        url_prefix='/admin')


@admin_panel.route('/')
@login_required
def admin():
    add_object_form = forms.Object()
    mod_object_form = forms.Modify_Object()
    add_cat_form = forms.Category()
    mod_cat_form = forms.Modify_Category()
    mod_isl_form = forms.Modify_Island()

    add_user_form = forms.Add_User()
    mod_user_form = forms.Modify_User()
    return render_template('admin.html',
                           add_object_form=add_object_form,
                           mod_object_form=mod_object_form,
                           add_cat_form=add_cat_form,
                           mod_cat_form=mod_cat_form,
                           mod_isl_form=mod_isl_form,
                           add_user_form=add_user_form,
                           mod_user_form=mod_user_form,

                           user=current_user
                           )


''' Object handlers '''


@admin_panel.route('/add_object', methods=['GET', 'POST'])
@login_required
def add_object():
    form = forms.Object()
    if form.validate_on_submit():
        nfn = None
        if form.img.has_file():
            valid_files = ['jpg', 'png', 'gif', 'jpeg']
            pic = form.img.data
            h = hashlib.sha1(repr(time.time()).encode('utf-8')).hexdigest()
            ext = pic.filename.rsplit('.', 1)[1]
            if ext.lower() not in valid_files:
                flash('Verkeerde bestands type', 'error')
                return redirect(url_for('admin_panel.admin'))
            nfn = '{}.{}'.format(h, ext)
            pic.save(os.path.join(
                app.instance_path, 'uploads', nfn
            ))
        print(form.date_from.data)
        print(form.date_to.data)
        if not (form.date_from and form.date_to) and\
           not form.always.data:
            flash('Geen periode geselecteerd', 'error')
            return redirect(url_for('admin_panel.admin'))
        island = Island.query.filter(Island.name == form.island.data).first()
        cat = form.category.data

        dateFrom = None
        dateTo = None
        if not form.always.data:
            dateFrom = datetime.datetime.strptime(form.date_from.data, '%d-%m-%Y')
            dateTo = datetime.datetime.strptime(form.date_to.data, '%d-%m-%Y')

        o = Object(
                form.name.data,
                form.desc.data,
                nfn,
                dateFrom,
                dateTo,
                cat,
                island,
                always=form.always.data,
        )
        db.session.add(o)
        db.session.commit()
        flash('Object {} toegevoegd.'.format(form.name.data))
    return redirect(url_for('admin_panel.admin'))


@admin_panel.route('/modify_object', methods=['GET', 'POST'])
@login_required
def modify_object():
    form = forms.Modify_Object()
    if form.validate_on_submit():
        obj = form.name.data
        cur_obj = obj.name
        if form.remove.data:
            db.session.delete(obj)
            flash('{} is verwijderd.'.format(cur_obj))
        else:
            if form.img.has_file():  # New file uploaded for image
                valid_files = ['jpg', 'png', 'gif', 'jpeg']
                pic = form.img.data
                h = hashlib.sha1(repr(time.time()).encode('utf-8')).hexdigest()
                ext = pic.filename.rsplit('.', 1)[1]
                if ext.lower() not in valid_files:
                    flash('Verkeerde bestands type', 'error')
                    return redirect(url_for('admin_panel.admin'))
                nfn = '{}.{}'.format(h, ext)
                pic.save(os.path.join(
                    app.instance_path, 'uploads', nfn
                ))
                obj.img = nfn
            if form.new_name.data:
                obj.name = form.new_name.data
            if form.desc.data:
                obj.desc = form.desc.data
            if form.date_from.data:
                dateFrom = datetime.datetime.strptime(form.date_from.data, '%d-%m-%Y')
                obj.date_from = dateFrom
            if form.date_to.data:
                dateTo = datetime.datetime.strptime(form.date_to.data, '%d-%m-%Y')
                obj.date_to = dateTo
            if form.always.data:
                obj.always = form.always.data
            if form.category.data != obj.category.id:
                cat = form.category.data
                obj.category = cat
            if form.island.data != obj.island.name:
                isl = Island.query.\
                        filter(Island.name == form.island.data).first()
                obj.island = isl
            flash('{} is aangepast.'.format(cur_obj))
        db.session.commit()
    return redirect(url_for('admin_panel.admin'))


''' Category handlers '''


@admin_panel.route('/add_category', methods=['GET', 'POST'])
@login_required
def add_category():
    form = forms.Category()
    if form.validate_on_submit():
        cat = Category.query.filter(Category.name == form.name.data).first()
        if cat is None:
            cat = Category(form.name.data)

            if not form.img.has_file():
                flash('Icoon benodigd', 'error')
                return redirect(url_for('admin_panel.admin'))
            valid_files = ['jpg', 'png', 'gif', 'jpeg']
            pic = form.img.data
            h = hashlib.sha1(repr(time.time()).encode('utf-8')).hexdigest()
            ext = pic.filename.rsplit('.', 1)[1]
            if ext.lower() not in valid_files:
                flash('Verkeerde bestands type', 'error')
                return redirect(url_for('admin_panel.admin'))
            nfn = '{}.{}'.format(h, ext)
            pic.save(os.path.join(
                app.instance_path, 'uploads', nfn
            ))
            cat.img = nfn

            db.session.add(cat)
            db.session.commit()
            flash('Categorie {} toegevoegd.'.format(form.name.data))
        else:
            flash('Categorie {} bestaat al.'.format(form.name.data), 'error')
    return redirect(url_for('admin_panel.admin'))


@admin_panel.route('/modify_category', methods=['GET', 'POST'])
@login_required
def modify_category():
    form = forms.Modify_Category()
    if form.validate_on_submit():
        cat = form.category.data
        if cat is None:
            flash('Error..?', 'error')
            return redirect(url_for('admin_panel.admin'))
        cur_cat = cat.name
        if form.remove.data:
            if len(cat.objects) > 0:
                flash('{} kan niet worden verwijderd.\
                       Er zijn nog objecten in deze categorie'.format(cur_cat))
            else:
                db.session.delete(cat)
                flash('{} is verwijderd.'.format(cur_cat))
        else:
            if form.name.data:
                cat.name = form.name.data
            if form.img.has_file():
                valid_files = ['jpg', 'png', 'gif', 'jpeg']
                pic = form.img.data
                h = hashlib.sha1(repr(time.time()).encode('utf-8')).hexdigest()
                ext = pic.filename.rsplit('.', 1)[1]
                if ext.lower() not in valid_files:
                    flash('Verkeerde bestands type', 'error')
                    return redirect(url_for('admin_panel.admin'))
                nfn = '{}.{}'.format(h, ext)
                pic.save(os.path.join(
                    app.instance_path, 'uploads', nfn
                ))
                cat.img = nfn

            flash('{} is aangepast.'.format(cur_cat))
        db.session.commit()
    return redirect(url_for('admin_panel.admin'))


''' Island handlers '''


@admin_panel.route('/modify_island', methods=['GET', 'POST'])
@login_required
def modify_island():
    form = forms.Modify_Island()
    if form.validate_on_submit():
        isl = Island.query.filter(Island.name == form.island.data).first()
        if isl is None:
            flash('Error..?', 'error')
            return redirect(url_for('admin_panel.admin'))
        else:
            isl.desc = form.desc.data
            isl.car = form.car.data
            isl.airplane = form.airplane.data
            isl.color = form.color.data
            print(form.img.data)
            if form.img.has_file():  # New file uploaded for image
                valid_files = ['jpg', 'png', 'gif', 'jpeg']
                pic = form.img.data
                h = hashlib.sha1(repr(time.time()).encode('utf-8')).hexdigest()
                ext = pic.filename.rsplit('.', 1)[1]
                if ext.lower() not in valid_files:
                    flash('Verkeerde bestands type', 'error')
                    return redirect(url_for('admin_panel.admin'))
                nfn = '{}.{}'.format(h, ext)
                pic.save(os.path.join(
                    app.instance_path, 'uploads', nfn
                ))
                isl.img = nfn

            flash('{} is aangepast.'.format(isl.name))
            db.session.commit()
    return redirect(url_for('admin_panel.admin'))


''' User handlers '''


@admin_panel.route('/add_user', methods=['GET', 'POST'])
@login_required
def add_user():
    form = forms.Add_User()
    if form.validate_on_submit():
        usr = User.query.filter(User.username == form.username.data).first()
        if usr:
            flash('Gebruikersnaam is al in gebruik.')
            return redirect(url_for('admin_panel.admin'))
        hashed = bcrypt.hashpw(form.password.data.encode('UTF-8'),
                               bcrypt.gensalt())
        usr = User(form.username.data, hashed,
                   request.remote_addr)
        db.session.add(usr)
        db.session.commit()
        flash('Gebruiker {} is toegevoegd'.format(form.username.data))
    return redirect(url_for('admin_panel.admin'))


@admin_panel.route('/modify_user', methods=['GET', 'POST'])
@login_required
def modify_user():
    form = forms.Modify_User()
    if form.validate_on_submit():
        usr = form.user.data
        if usr is None:
            flash('Error..?', 'error')
            return redirect(url_for('admin_panel.admin'))
        cur_usr = usr.username
        if form.remove.data:
            if usr.username == 'admin':
                flash('\'admin\' account kan niet worden verwijderd (tegen jezelf buitensluiten)', 'error')
            else:
                db.session.delete(usr)
                flash('{} is verwijderd.'.format(cur_usr))
        else:
            if form.username.data:
                usr.username = form.username.data
            if form.password.data:
                hashed = bcrypt.hashpw(form.password.data.encode('UTF-8'),
                                       bcrypt.gensalt())
                usr.password = hashed
            flash('Gebruiker {} is aangepast'.format(cur_usr))
        db.session.commit()
    return redirect(url_for('admin_panel.admin'))


''' Login & Logout handlers '''


@admin_panel.route('/login', methods=['GET', 'POST'])
def login():
    form = forms.Login()
    if request.method == 'GET':
        return render_template('login.html', form=form)
    username = form.username.data
    password = form.password.data
    user = User.query.filter(User.username == username).first()
    if user is None:
        flash('Gebruikersnaam of wachtwoord ongeldig.', 'error')
        return redirect(url_for('admin_panel.login'))
    hashed = user.password
    if not bcrypt.checkpw(password.encode('UTF-8'), hashed):
        flash('Gebruikersnaam of wachtwoord ongeldig.', 'error')
        return redirect(url_for('admin_panel.login'))
    login_user(user)
    return redirect(url_for('admin_panel.admin'))


@admin_panel.route('/logout')
def logout():
    logout_user()
    flash("Succesvol uitgelogd.")
    return redirect(url_for('home'))
