from app import app, db
from app.models import User, Island, Object, Category
from app.utils import inPeriod, scale
from flask import Blueprint, jsonify, redirect, request, url_for, session
from flask_login import login_user, logout_user, login_required, current_user
import datetime
import operator

rest = Blueprint('api', __name__, url_prefix='/api')


@rest.errorhandler(404)
def notfound():
    message = {
            'status': '404',
            'message': 'Not Found: ' + request.url
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@rest.errorhandler(401)
def unauthorized(e='401: Unauthorized'):
    message = {
            'status': '401',
            'message': 'Unauthorized'
            # 'info': 'You should log in into the administrator panel'
    }
    resp = jsonify(message)
    resp.status_code = 401
    return resp


def invalid(var, val):
    message = {
            'status': '400: Bad Request',
            'message': 'Not valid: {{"{}":"{}"}}'.format(var, val)
    }
    resp = jsonify(message)
    resp.status_code = 400
    return resp


@rest.route('/')
def api():
    message = {
            'url': request.url,
            'resources': {
                'eiland': {
                    'url': request.url + 'eiland'
                },
                'user': {
                    'url': request.url + 'user'
                },
                'object': {
                    'url': request.url + 'object'
                },
                'categorie': {
                    'url': request.url + 'categorie'
                }
            }
    }
    resp = jsonify(message)
    return resp


@rest.route('/user')
def user():
    if not current_user.is_authenticated:
        return unauthorized()
    message = {
            'user': current_user.username,
            'registered_on': current_user.registered_on,
            'ip': current_user.ip
    }
    resp = jsonify(message)
    return resp


@rest.route('/eiland/<island>')
@rest.route('/eiland/')
@rest.route('/eiland')
def island(island=None):
    if not island:
        message = {
            'usage': [
                '/api/eiland/<eiland>'
                ]
        }
        return jsonify(message)
    isl = Island.query.filter(Island.name == island).first()
    if not isl:
        return notfound()

    message = {
            'id': isl.id,
            'name': isl.name,
            'desc': isl.desc,
            'car': isl.car,
            'airplane': isl.airplane,
            'color': isl.color,
            'img': isl.img,
            'objects': [
                {
                    'id': o.id,
                    'name': o.name
                } for o in isl.objects
            ]
    }
    return jsonify(message)


@rest.route('/object', methods=['GET', 'POST', 'PUT'])
@rest.route('/object/<oid>')
def object(oid=None):
    if request.method == 'GET':
        if not oid:
            message = {
                    'GET': '/object/<oid>',
                    'GET_extra': '/object/all',
                    'POST': '/object',
                    'data': {
                        'name': 'Name of object',
                        'desc': 'Description of object',
                        'img': 'Img url/file of for object',
                        'date_from': 'Starting date of object',
                        'date_to': 'Ending date of object',
                        'always': 'Whether object is always valid',
                        'category': 'Category of object',
                        'island': 'Island where object belongs to'
                    }
            }
            return jsonify(message)
        else:
            if oid == 'all':
                objects = Object.query.all()
                message = []
                for obj in objects:
                    message.append({
                            'id': obj.id,
                            'name': obj.name,
                            'desc': obj.desc,
                            'img': obj.img,
                            'date_from': obj.date_from.strftime('%d-%m-%Y') if obj.date_from else obj.date_from,
                            'date_to': obj.date_to.strftime('%d-%m-%Y') if obj.date_to else obj.date_to,
                            'always': obj.always,
                            'category': {
                                'id': obj.category.id,
                                'name': obj.category.name
                            } if obj.category else None,
                            'island': obj.island.name
                        }
                    )
            else:
                obj = Object.query.filter(Object.id == oid).first()
                if not obj:
                    return notfound()
                message = {
                        'id': obj.id,
                        'name': obj.name,
                        'desc': obj.desc,
                        'img': obj.img,
                        'date_from': obj.date_from.strftime('%d-%m-%Y') if obj.date_from else obj.date_from,
                        'date_to': obj.date_to.strftime('%d-%m-%Y') if obj.date_to else obj.date_to,
                        'always': obj.always,
                        'category': {
                            'id': obj.category.id,
                            'name': obj.category.name
                        } if obj.category else None,
                        'island': obj.island.name
                }
            return jsonify(message)
    if request.method == 'POST':
        if not request.is_json:
            message = {
                    'error': 'JSON Data needed'
            }
            return jsonify(message)
        json = request.json
        isl = Island.query.filter(Island.name == json['island']).first()
        if not isl:
            return invalid('island', json['island'])
        try:
            dateFrom = datetime.datetime.fromtimestamp(int(json['date_from']))
            dateTo = datetime.datetime.fromtimestamp(int(json['date_to']))
            obj = Object(json['name'], json['desc'], json['img'],
                         dateFrom, dateTo, json['type'], isl)
            db.session.add(obj)
            db.session.commit()
        except KeyError as e:
            return invalid(str(e), 'not given')
        return str(json)


@rest.route('/categorie/<catid>')
@rest.route('/categorie')
def category(catid=None):
    if not catid:
        message = {
                'usage': [
                    '/category/all',
                    '/category/<catid>'
                    ]
                }
        return jsonify(message)
    if catid == 'all':
        cats = Category.query.all()
        message = {
                'categories': [
                    {
                        'id': cat.id,
                        'name': cat.name,
                        'icon': cat.img,
                        'objects': [
                            {
                                'id': obj.id,
                                'name': obj.name
                            } for obj in cat.objects
                        ]
                    } for cat in cats
                ]
        }
        return jsonify(message)
    cat = Category.query.filter(Category.id == catid).first()
    if not cat:
        return invalid('catid', 'not found')
    message = {
            'id': cat.id,
            'name': cat.name,
            'icon': cat.img,
            'objects': [
                {
                    'id': obj.id,
                    'name': obj.name
                } for obj in cat.objects
            ]
    }
    return jsonify(message)


@rest.route('/search/<int:dateFrom>/<int:dateTo>')
@rest.route('/search')
def search(dateFrom=None, dateTo=None):
    if not dateFrom or not dateTo:
        message = {
                'usage': '/search/<dateFrom>/<dateTo>'
        }
        return jsonify(message)

    message = {
            'objects': []
    }
    try:
        dateFrom = datetime.datetime.fromtimestamp(dateFrom)
        dateTo = datetime.datetime.fromtimestamp(dateTo)
        objects = Object.query.all()
        for obj in objects:
            if obj.always or inPeriod(dateFrom, dateTo,
                                      obj.date_from, obj.date_to):
                message['objects'].append({
                        'id': obj.id,
                        'name': obj.name,
                        'desc': obj.desc,
                        'img': obj.img,
                        'date_from': obj.date_from.strftime('%d-%m-%Y'),
                        'date_to': obj.date_to.strftime('%d-%m-%Y'),
                        'always': obj.always,
                        'category': {
                            'id': obj.category.id,
                            'name': obj.category.name
                        } if obj.category else None,
                        'island': obj.island.name
                })
    except Exception as e:
        return invalid('error', str(e))
    return jsonify(message)


@rest.route('/getcolors/<dateFrom>/<dateTo>/<cats>')
@rest.route('/getcolors')
def getcolor(island=None, dateFrom=None, dateTo=None, cats=None):
    if not cats:
        message = {
                'usage': '/getcolor/<dateFrom>/<dateTo>/<categories>'
        }
        return jsonify(message)
    islands = Island.query.all()
    cats = [int(i) for i in cats.split(',')]
    island_dict = {}

    if dateFrom == 'NaN' or dateTo == 'NaN':
        for i in islands:
            island_dict[i.name] = 0
            for obj in i.objects:
                if obj.category.id in cats:
                    island_dict[i.name] += 1
    else:
        dateFrom = datetime.datetime.fromtimestamp(int(dateFrom))
        dateTo = datetime.datetime.fromtimestamp(int(dateTo))
        for i in islands:
            island_dict[i.name] = 0
            for obj in i.objects:
                if obj.always or inPeriod(dateFrom, dateTo,
                                          obj.date_from, obj.date_to):
                    if obj.category and obj.category.id in cats:
                        island_dict[i.name] += 1

    src = (0, max(island_dict.values()))
    dst = (0, 100)
    for i, val in island_dict.items():
        sf = scale(val, src, dst)/100
        sf_red = 1-sf
        island_dict[i] = {
                'r': round(255*sf_red),
                'g': round(255*sf),
                'b': 0
        }
    return jsonify(island_dict)
