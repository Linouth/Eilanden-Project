from . import db
from datetime import datetime


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True, index=True)
    password = db.Column(db.String)
    # email = db.Column(db.String(50), unique=True, index=True)
    registered_on = db.Column(db.DateTime)
    ip = db.Column(db.String(46))
    # invite_key = db.Column(db.String(10))
    privileges = db.Column(db.Integer)

    def __init__(self, username, password, ip, priv=0):
        self.username = username
        self.password = password
        self.registered_on = datetime.utcnow()
        self.ip = ip
        self.priv = priv

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % self.username


class Object(db.Model):
    __tablename__ = 'objects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    desc = db.Column(db.String)
    img = db.Column(db.String)  # Filename
    date_from = db.Column(db.DateTime)
    date_to = db.Column(db.DateTime)
    always = db.Column(db.Boolean)

    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    category = db.relationship('Category', back_populates='objects')

    island_id = db.Column(db.Integer, db.ForeignKey('islands.id'))
    island = db.relationship('Island', back_populates='objects')

    def __init__(self, name, desc, img, date_from, date_to, category, island,
                 always=False):
        self.name = name
        self.desc = desc
        self.img = img
        self.date_from = date_from if date_from else None
        self.date_to = date_to if date_from else None
        self.category = category
        # self.extra_info = link
        self.always = always

        self.island = island


class Island(db.Model):
    __tablename__ = 'islands'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), index=True, unique=True)
    desc = db.Column(db.String())
    car = db.Column(db.Boolean(), default=False)
    airplane = db.Column(db.Boolean(), default=False)
    color = db.Column(db.String())
    img = db.Column(db.String)  # Filename

    objects = db.relationship('Object', order_by=Object.id,
                              back_populates='island')

    def __init__(self, name):
        self.name = name


class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), index=True, unique=True)
    img = db.Column(db.String)  # Filename

    objects = db.relationship('Object', order_by=Object.id,
                              back_populates='category')

    def __init__(self, name):
        self.name = name
