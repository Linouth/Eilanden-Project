from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flaskext.markdown import Markdown
import os


app = Flask(__name__)

app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True

app.config.update(dict(
    SECRET_KEY='SomethingRandom',
    SQLALCHEMY_DATABASE_URI='sqlite:///' + os.path.join(
        app.instance_path, 'database.db'
    ),
    SQLALCHEMY_TRACK_MODIFICATIONS=False
))

db = SQLAlchemy(app)
from .models import User, Island, Object, Category
db.create_all()

Markdown(app)

from .blueprints.admin_panel.views import admin_panel
from .blueprints.rest.views import rest
app.register_blueprint(admin_panel)
app.register_blueprint(rest)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'admin_panel.login'
login_manager.login_message = 'Log in om deze pagina te bereiken.'


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


@app.cli.command('initdb')
def initdb_command():
    ''' Initialize database and create tables '''
    import bcrypt

    print('Clearing, initializing and populating database')
    db.drop_all()
    db.create_all()

    # Populate database
    # Obviously change default password...
    hashed = bcrypt.hashpw('admin'.encode('UTF-8'), bcrypt.gensalt())
    user = User('admin', hashed, '127.0.0.1')
    db.session.add(user)

    eilanden = ['texel', 'vlieland', 'terschelling',
                'ameland', 'schiermonnikoog']
    for e in eilanden:
        eiland = Island(e)
        eiland.desc = 'Het eiland ' + e.title()
        eiland.color = '#64B5F6'
        db.session.add(eiland)
    db.session.commit()


@login_manager.user_loader
def load_user(id):
    from .models import User
    return User.query.get(int(id))


# Filters


def datetimef(val, format='%d-%m-%Y'):
    return val.strftime(format)


app.jinja_env.filters['datetimef'] = datetimef


from . import views
