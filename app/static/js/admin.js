$('.option', 'section#sidebar').click(function() {
    var id = this.id;

    // Switch sidebar selected tab
    $('.option', 'section#sidebar').removeClass('active');
    $(this).addClass('active');

    $('div.tab', 'section#container').removeClass('active');
    $('#'+id, 'section#container').addClass('active');
});


/*
 * Function and listener to live update values in admin panel
 * 'Eilanden' Tab
 */
function update_island_fields(island) {
    $.getJSON( '../api/eiland/' + island, '',
        function(data) {
            $('#desc', '#modify_island').val(data.desc);
            $('#airplane', '#modify_island').prop('checked', data.airplane);
            $('#car', '#modify_island').prop('checked', data.car);

            $('#color', '#modify_island').val(data.color);
            update_isl_col($('#color', '#modify_island'));
        }
    );
}
function update_isl_col(field) {
    console.log(field);
    $(field).css('border-color', $(field).val());
}
update_island_fields('texel');
$('#island', '#modify_island').change(function() {
    update_island_fields($(this).val());
});
$('#color', '#modify_island').bind('input propertychange', function() {
    update_isl_col(this);
});


/*
 * Function and listener to live update values in admin panel
 * 'Objecten' Tab
 */
function update_object_fields(oid) {
    $.getJSON( '../api/object/' + oid, '',
        function(data) {
            var form = '#modify_object';
            $('#new_name', form).val(data.name);
            $('#desc', form).val(data.desc);
            $('#date_from', form).val(data.date_from);
            $('#date_to', form).val(data.date_to);
            $('#always', form).prop('checked', data.always);
            $('#category', form).val(data.category.id);
            $('#island', form).val(data.island);

            if (data.img) {
                // $('div.form-label:contains("Plaatje")', form).append(' <a href="../img/uploads/' + data.img + '">view</a>');
            }
        }
    );
}
if ($('#name', '#modify_object').val() != null) {
    update_object_fields(1);
}
$('#name', '#modify_object').change(function() {
    update_object_fields($(this).val());
});
