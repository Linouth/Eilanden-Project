$( function() {
    $('.datepicker').datepicker ({
        dateFormat: 'dd-mm-yy',
        onSelect: onDateSelect
    });

    $('input.check', '#form').change(function() {
        updateIslands();
    });

    $('svg', 'section#islands').click(function() {
        var dateFrom = $('.datepicker:eq(0)').val().split('-');
        var dateTo = $('.datepicker:eq(1)').val().split('-');
        dateFrom = (new Date(dateFrom[2], dateFrom[1]-1, dateFrom[0])).getTime()/1000;
        dateTo = (new Date(dateTo[2], dateTo[1]-1, dateTo[0])).getTime()/1000;

        if (!isNaN(dateFrom) && !isNaN(dateTo)) {
            window.location = 's/' + $(this).attr('id') + '/' + dateFrom + '/' + dateTo;
        } else {
            window.location = 's/' + $(this).attr('id');
        }
    });

	$("#leesmeer").click(function() {
		$("#act").toggleClass('more_info')
		$("#activiteiten").toggleClass('more_info')
		if ($("#act").hasClass('more_info')) {
			$(this).val("Lees minder");
		} else {
			$(this).val("Lees meer");
		}
	});


    var clouds = [];
    $('svg', 'section#clouds').each(function() {
        clouds.push(new Cloud($(this)));
    });
    var t = setInterval(function() {
        for (c in clouds) {
            clouds[c].update();
        }
    }, 1000/60);



    /* Updates */
    updateIslands();
});

function onDateSelect(date, obj){
    updateIslands();
}

function updateIslands() {
    var dateFrom = $('.datepicker:eq(0)').val().split('-');
    var dateTo = $('.datepicker:eq(1)').val().split('-');
    dateFrom = (new Date(dateFrom[2], dateFrom[1]-1, dateFrom[0])).getTime()/1000;
    dateTo = (new Date(dateTo[2], dateTo[1]-1, dateTo[0])).getTime()/1000;

    var cats = [];
    $('input.check:checked', '#form').each(function() {
        cats.push($(this).attr('name'));
    });

    var col;
    if (cats.length > 0) {
        $.get('api/getcolors/' + dateFrom + '/' + dateTo + '/' + cats.join(), function(data) {
            for (isl in data){
                col = 'rgb('+data[isl].r+','+data[isl].g+','+data[isl].b+')';
                $('#'+isl).css('fill', col);
            }
        });
    } else if (cats.length <= 0) {
        $('svg', 'section#islands').each(function() {
            $(this).css('fill', '#ffeead');
        });
    }
}

class Cloud {
    constructor(el) {
        this.el = el;
        this.scale = this.rand(0.5, 1);
        this.vx = this.rand(1, 1.5);
        this.vy = this.rand(0.5, 1.2);

        this.x_max = $('#clouds').width();
        this.y_max = $('#clouds').height();
        this.x = this.rand(-500, this.x_max);
        this.y = this.rand(-500, this.y_max);
        this.out_range = 300;

        this.el.css('transform', 'scale('+this.scale+')');
        this.update_pos();
        this.el.css('display', 'block');
    }

    update() {
        this.x += this.vx;
        this.y += this.vy

        if (this.x > this.x_max + this.out_range) {
            this.x = -this.out_range;
        }
        if (this.y > this.y_max + this.out_range) {
            this.y = -this.out_range;
        }
        this.update_pos();
    }

    rand(min, max) {
        return Math.random() * (max - min) + min;
    }

    update_pos() {
        this.el.css('left', this.x + 'px');
        this.el.css('top', this.y + 'px');
    }
}
