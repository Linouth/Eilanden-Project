from . import app
from .utils import inPeriod
from .models import User, Island, Object, Category
from flask import (
        render_template, redirect, flash, request, url_for, session,
        send_from_directory
)
import datetime


@app.route('/')
def home():
    cats = Category.query.all()
    return render_template('index.html', cats=cats)


@app.route('/img')
def img():
    return 'images'


@app.route('/img/uploads/<img>')
def img_uploaded(img):
    return send_from_directory('../instance/uploads/', img)


''' Main pages '''


@app.route('/s/<island>/<int:dateFrom>/<int:dateTo>')
@app.route('/s/<island>/')
@app.route('/s/<island>')
def island(island=None, dateFrom=None, dateTo=None):
    isl = Island.query.filter(Island.name == island).first()
    if not isl:
        return redirect(url_for('home'))

    # All Objects
    if not dateFrom or dateFrom == 'NaN' or\
       not dateTo or dateTo == 'NaN':
           return render_template('objects.html', isl=isl, objects=sorted(isl.objects, key=lambda x: x.category.id))

    dateFrom = datetime.datetime.fromtimestamp(int(dateFrom))
    dateTo = datetime.datetime.fromtimestamp(int(dateTo))
    out = []
    for o in isl.objects:
        if o.always or inPeriod(dateFrom, dateTo, o.date_from, o.date_to):
            out.append(o)
    return render_template('objects.html', isl=isl, objects=sorted(out, key=lambda x: x.category.id))


@app.route('/o/<int:oid>')
@app.route('/o/<int:oid>/')
@app.route('/o/<int:oid>/<we>')
def object(oid, we=None):
    obj = Object.query.filter(Object.id == oid).first()
    if not obj:
        return 'Object niet gevonden'
    return render_template('object_info.html', o=obj)
