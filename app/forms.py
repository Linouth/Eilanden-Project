from app.models import Island as sql_isl
from app.models import Category as sql_cat
from app.models import Object as sql_obj
from app.models import User as sql_usr
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import (
        StringField, PasswordField, BooleanField, SelectField
)
from wtforms.fields import SubmitField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.widgets import TextArea
from wtforms.validators import DataRequired


def query_factory(model=sql_obj):
    return model.query.all()


class Login(FlaskForm):
    username = StringField('Gebruikersnaam', validators=[DataRequired()])
    password = PasswordField('Wachtwoord', validators=[DataRequired()])


class Object(FlaskForm):
    name = StringField('Naam', validators=[DataRequired()])
    desc = StringField('Beschrijving', widget=TextArea())
    img = FileField('Plaatje')
    date_from = StringField('Begin datum', render_kw={'class': 'datepicker'})
    date_to = StringField('Eind datum', render_kw={'class': 'datepicker'})
    always = BooleanField('Altijd beschikbaar')

    category = QuerySelectField('Categorie',
                                query_factory=lambda: query_factory(sql_cat),
                                get_label='name')

    islands = sql_isl.query.all()
    choices = [(x.name, x.name.title()) for x in islands]
    island = SelectField('Eiland', choices=choices)

    submit = SubmitField(label='Submit')


class Modify_Object(FlaskForm):
    name = QuerySelectField('Object', validators=[DataRequired()],
                            query_factory=lambda: query_factory(sql_obj),
                            get_label='name')

    new_name = StringField('Nieuwe naam')
    desc = StringField('Beschrijving', widget=TextArea())
    # img = StringField('Plaatje (url)')
    img = FileField('Plaatje')
    date_from = StringField('Begin datum', render_kw={'class': 'datepicker'})
    date_to = StringField('Eind datum', render_kw={'class': 'datepicker'})
    always = BooleanField('Altijd beschikbaar')

    category = QuerySelectField('Categorie',
                                query_factory=lambda: query_factory(sql_cat),
                                get_label='name')

    islands = sql_isl.query.all()
    choices = [(x.name, x.name.title()) for x in islands]
    island = SelectField('Eiland', choices=choices)

    submit = SubmitField(label='Submit')
    remove = SubmitField(label='Verwijderen', render_kw={'class': 'delete'})


class Category(FlaskForm):
    name = StringField('Naam', validators=[DataRequired()])
    desc = StringField('Beschrijving')
    img = FileField('Icoon')
    submit = SubmitField(label='Submit')


class Modify_Category(FlaskForm):
    category = QuerySelectField('Categorie',
                                query_factory=lambda: query_factory(sql_cat),
                                get_label='name')

    name = StringField('Nieuwe naam')
    desc = StringField('Beschrijving')
    img = FileField('Icoon')

    submit = SubmitField(label='Submit')
    remove = SubmitField(label='verwijderen', render_kw={'class': 'delete'})


class Modify_Island(FlaskForm):
    islands = sql_isl.query.all()
    choices = [(x.name, x.name.title()) for x in islands]
    island = SelectField('Eiland', validators=[DataRequired()],
                         choices=choices)

    desc = StringField('Beschrijving', widget=TextArea())
    img = FileField('Plaatje')
    car = BooleanField('Auto\'s toegestaan')
    airplane = BooleanField('Vliegveld aanwezig')
    color = StringField('Kleur')

    submit = SubmitField(label='Submit')


class Add_User(FlaskForm):
    username = StringField('Gebruikersnaam', validators=[DataRequired()])
    password = PasswordField('Wachtwoord', validators=[DataRequired()])

    submit = SubmitField(label='Submit')


class Modify_User(FlaskForm):
    user = QuerySelectField('User',
                            query_factory=lambda: query_factory(sql_usr),
                            get_label='username')

    username = StringField('Nieuwe Gebruikersnaam')
    password = PasswordField('Nieuwe Wachtwoord')

    submit = SubmitField(label='Submit')
    remove = SubmitField(label='verwijderen', render_kw={'class': 'delete'})
