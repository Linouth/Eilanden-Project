import sys


def usage():
    print('USAGE: {} (initdb|serve)'.format(sys.argv[0]))
    sys.exit(1)


def serve():
    from app import app
    from livereload import Server
    app.debug = True

    server = Server(app.wsgi_app)

    server.watch('app/templates/*.html')
    server.watch('app/static/*')
    server.serve()


def initdb():
    from flask_sqlalchemy import SQLAlchemy
    from app.models import User, Island, Object, Category
    pass


if __name__ == '__main__':
    if not len(sys.argv) > 1:
        usage()
    if sys.argv[1] == 'initdb':
        initdb()
    elif sys.argv[1] == 'serve':
        serve()
    else:
        usage()
