var gulp            = require('gulp'),
    imagemin        = require('gulp-imagemin'),
    uglify          = require('gulp-uglify'),
    autoprefixer    = require('gulp-autoprefixer'),
    sass            = require('gulp-sass'),
    plumber         = require('gulp-plumber');


var paths = {
    css:    ['src/css/**/*.scss', '!./src/css/**/__*.scss'],
    html:   'src/*.html',
    js:     'src/js/*.js',
    img:    'src/img/*',
}

var output = {
    html:       'app/templates/',
    statics:    'app/static/'
}


// Task html
gulp.task('html', function() {
    gulp.src(paths.html)
        .pipe(gulp.dest(output.html));
});

// Task ja
gulp.task('js', function() {
    gulp.src(paths.js)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest(output.statics + 'js'));
});

// Task image
gulp.task('image', function() {
    gulp.src(paths.img)
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(gulp.dest(output.statics + 'img'));
});

// Task css
gulp.task('style', function() {
    gulp.src(paths.css)
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulp.dest(output.statics + 'css'));
});

// Task watch
gulp.task('watch', function() {
    gulp.watch(paths.css, ['style']);
    gulp.watch(paths.html, ['html']);
    gulp.watch(paths.img, ['image']);
    gulp.watch(paths.js, ['js']);
});

//Task Default
gulp.task('default', ['html', 'style', 'js', 'image', 'watch'])
